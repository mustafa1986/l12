import java.util.Scanner;

public class Task2 {
    //2)daxil edilmiş sətirdə başlanğıc hərflə son hərfi eyni olan sözləri qaytarsın(sözlər boşluq simvolu ilə ayrılmışdır)

    private static String[] inputString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        String[] sozler = metn.split(" ");
        return sozler;
    }

    private static String checkWord(String str1) {
        char firstSymbol = str1.charAt(0);
        char lastSymbol = str1.charAt(str1.length() - 1);
        if (firstSymbol == lastSymbol) {
            return str1;
        }
        return null;

    }

    public static void printWord() {
        String[] sozler= inputString();
        String result="";
        for (int i = 0; i < sozler.length; i++) {
            if(checkWord(sozler[i])==null){
                continue;
            }else {
                result=result + ","+sozler[i];
            }

        }
        if (result.length()<=2){
            System.out.println("Daxil etdiyiniz metnde başlanğıc hərflə son hərfi eyni olan söz yoxdur");
        }else
            System.out.println("başlanğıc hərflə son hərfi eyni olan sözler: " + result);
    }

}
