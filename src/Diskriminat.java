import java.util.Scanner;

public class Diskriminat {
    private static void menu(String str1) {
        System.out.println(str1);
    }

    private static int inputUser(String metn) {
        boolean isError = true;
        menu(metn);
        int numberA = 0;
        Scanner scanner = new Scanner(System.in);
        while (isError) {
            if (scanner.hasNextInt()) {
                numberA = scanner.nextInt();
                isError = false;
            } else {
                System.out.println("yalniz reqem daxil edin: ");
                scanner.next();
            }
        }
        return numberA;
    }

    public static double[] checkDiskriminat() {
        double numberA = inputUser("A ededin daxil edin: ");
        double numberB = inputUser("B ededin daxil edin: ");
        double numberC = inputUser("C ededin daxil edin: ");
        double diskriminat = (numberB * numberB) - (4 * numberA * numberC);
        double kokAlti=Math.sqrt(diskriminat);
        double[] x=new double[2];

        if (numberA == 0 && numberB == 0 && numberC == 0) {
            System.out.println("Kvadrat tenliyin helli yoxdu");
            System.exit(0);
        } else if (numberA == 0 && numberB == 0 && numberC != 0) {
            System.out.println("Kvadrat tenliyin helli yoxdu");
            System.exit(0);

        }if (diskriminat>0){
            x[0]=(-numberB+kokAlti)/(2*numberA);
            x[1]=(-numberB-kokAlti)/(2*numberA);
            
        } else if (diskriminat==0) {
            x[0]=(-numberB+kokAlti)/(2/numberA);
            
        }
        return x;

    }


}
